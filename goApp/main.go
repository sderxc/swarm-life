package main

import (
	"github.com/gen2brain/raylib-go/raygui"
	"github.com/gen2brain/raylib-go/raylib"
	"math"
	"math/rand"
	"time"
	"fmt"
)

const (
	WORLD_X = 1024
	WORLD_Y = 800
	SWARM_COUNT = 500
	BEE_SIZE = 2
	SPEED = 2
	RESOURCES_COUNT = 4
	RESOURCE_RADIUS = 20
	ROAR_RAIUS = 80
)
type Position struct {
	position rl.Vector2
}

type Counter struct {
	counters [RESOURCES_COUNT]int64
}

type Bee struct {
	Position
	Counter
	radius   float32
	direction rl.Vector2
	speed    float32
	targetResource int64
}

type Resource struct {
	Position
	radius   float32
	color rl.Color
}

type World struct {
	swarm [SWARM_COUNT]Bee
	resources [RESOURCES_COUNT]Resource
	test bool
}

func NewWorld() (w World) {
	w.Init()
	return
}

func getInitialDirection() rl.Vector2 {
	rand.Seed(time.Now().UnixNano())
	rX := randFloat(-1, 1);
	rY := randFloat(-1, 1);
	// rY := math.Sqrt(float64(1 - (rX*rX)))
	return rl.Vector2{float32(rX), float32(rY)}
}

func randColor() rl.Color {
	return rl.Color{uint8(rl.GetRandomValue(50, 255)),uint8(rl.GetRandomValue(50, 255)), uint8(rl.GetRandomValue(50, 255)), 255}
}

func randFloat(min, max float64) float64 {
	return min + rand.Float64() * (max - min)
}


func getDirectionTo(a rl.Vector2, b rl.Vector2) rl.Vector2 {

	toX := b.X - a.X
	toY := b.Y - a.Y
	toPointLength := getDistanceTo(a, b)

	if (toPointLength == 0) {
		return rl.Vector2{0, 0}
	} else {
		return rl.Vector2{float32(toX / toPointLength), float32(toY / toPointLength)}
	}
}

func getDistanceTo(a rl.Vector2, b rl.Vector2 ) float32 {
	toX := b.X - a.X
	toY := b.Y - a.Y
   	return float32(math.Sqrt(float64(toX * toX + toY * toY)))
}

// Init - initialize world
func (w *World) Init() {

	w.test = false;
	// swarm init
	for i := 0; i < SWARM_COUNT; i++ {
		w.swarm[i].position = rl.Vector2{float32(rl.GetRandomValue(1, WORLD_X)), float32(rl.GetRandomValue(1, WORLD_Y))}
		w.swarm[i].direction = getInitialDirection()
		w.swarm[i].speed = SPEED
		w.swarm[i].radius = BEE_SIZE
		w.swarm[i].targetResource = int64(rl.GetRandomValue(0, RESOURCES_COUNT - 1))
		for j := 0; j < RESOURCES_COUNT; j++ {
			w.swarm[i].counters[j] = 0
		}
	}

	// resources init
	for i := 0; i < RESOURCES_COUNT; i++ {
		w.resources[i].position = rl.Vector2{float32(rl.GetRandomValue(1, WORLD_X)), float32(rl.GetRandomValue(1, WORLD_Y))}
		w.resources[i].radius = RESOURCE_RADIUS
		w.resources[i].color = randColor()
	}
}
// communication 
// TODO реализовать сегментацию по пиксельной сетке
func (w *World) Roar(i int) {
for k := 0; k < SWARM_COUNT; k++ {
	if (getDistanceTo(w.swarm[i].position, w.swarm[k].position) <= ROAR_RAIUS) {
		anotherTarget := w.swarm[k].targetResource
		if ((w.swarm[i].counters[anotherTarget] + ROAR_RAIUS) < w.swarm[k].counters[anotherTarget]) {
			w.swarm[k].counters[anotherTarget] = w.swarm[i].counters[anotherTarget] + ROAR_RAIUS
			newDirection := getDirectionTo(w.swarm[k].position, w.swarm[i].position)
			w.swarm[k].direction.X = newDirection.X
			w.swarm[k].direction.Y = newDirection.Y
		}
	}
}
}
func (w *World) Update() {
	// Collision logic: bees vs walls
	for i := 0; i < SWARM_COUNT; i++ {
		if ((w.swarm[i].position.X + w.swarm[i].radius) >= WORLD_X) || ((w.swarm[i].position.X - w.swarm[i].radius) <= 0) {
			w.swarm[i].direction.X *= -1
		}
		if ((w.swarm[i].position.Y + w.swarm[i].radius) >= WORLD_Y) || ((w.swarm[i].position.Y - w.swarm[i].radius) <= 0) {
			w.swarm[i].direction.Y *= -1
		}

		// inc counters
		for j := 0; j < RESOURCES_COUNT; j++ {
			w.swarm[i].counters[j]++
		}

		// Collision logic: bees vs resouces
		currentTarget := w.swarm[i].targetResource
		if (getDistanceTo(w.swarm[i].position, w.resources[currentTarget].position) <= w.resources[currentTarget].radius) {
			w.swarm[i].direction.Y *= -1
			w.swarm[i].direction.X *= -1

			w.swarm[i].counters[currentTarget] = 0
			w.swarm[i].targetResource++

			if (w.swarm[i].targetResource == RESOURCES_COUNT) {
				w.swarm[i].targetResource = 0
			}
		}

		go w.Roar(i)

		w.swarm[i].position.X += w.swarm[i].direction.X * w.swarm[i].speed
		w.swarm[i].position.Y += w.swarm[i].direction.Y * w.swarm[i].speed

	}
}

func (w *World) Draw() {
	rl.BeginDrawing()
	rl.ClearBackground(rl.Black)

	for i := 0; i < SWARM_COUNT; i++ {
		rl.DrawCircleV(w.swarm[i].position, w.swarm[i].radius, w.resources[w.swarm[i].targetResource].color)
	}

	for i := 0; i < RESOURCES_COUNT; i++ {
		rl.DrawCircleV(w.resources[i].position, w.resources[i].radius, w.resources[i].color)
	}

	rl.EndDrawing()
}

func main() {
	rl.InitWindow(WORLD_X, WORLD_Y, "Swarm Life")

	rl.SetTargetFPS(30)

	world := NewWorld()

	for !rl.WindowShouldClose() {
		if (world.test == true) {
			world = NewWorld()
		}
		world.test = raygui.Button(rl.NewRectangle(0, 70, 80, 40), "Restart")
		rl.DrawText(fmt.Sprintf("FPS: %.0f", rl.GetFPS()), 1, 1, 40, rl.Blue)
		world.Update()

		world.Draw()
	}

	rl.CloseWindow()
}