export default class Recourse {
    constructor(sprite, x, y, name, color) {
        this.name = name;
        this.sprite = sprite;
        this.x = x;
        this.y = y;
        this.sprite.x = x;
        this.sprite.anchor.set(0.5);
        this.sprite.y = y;
        this.sprite.tint = color;
    }
}