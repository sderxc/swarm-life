export default class Item {
    constructor(sprite, worldX, worldY) {
        this.worldSize = {
            x: worldX,
            y: worldY,
        };
        this.sprite = sprite;
        this.speed = 1;
        this.initPosition();
        this.currRes = 'b';
        this.counters = {
            a: 0,
            b: 0,
            c: 0,
        };
    }
    initPosition() {
        this.sprite.x = Math.floor(Math.random() * this.worldSize.x);
        this.sprite.y = Math.floor(Math.random() * this.worldSize.y);

        this.sprite.vx = this.speed;
        this.sprite.vy = this.speed;
        this.goToCords(Math.floor(Math.random() * this.worldSize.x), Math.floor(Math.random() * this.worldSize.y));
    }

    move() {
        if (this.sprite.x < this.worldSize.x && this.sprite.x > 0) {
            this.lockX = false;
        }

        if (this.sprite.y < this.worldSize.y && this.sprite.y > 0) {
            this.lockY = false;
        }
        // отскок от границ мира
        if (!this.lockX && (this.sprite.x >= this.worldSize.x || this.sprite.x <= 0)) {
            this.sprite.vx = this.sprite.vx * -1;
            this.lockX = true;
        }

        if (!this.lockY && (this.sprite.y >= this.worldSize.y || this.sprite.y <= 0)) {
            this.sprite.vy = this.sprite.vy * -1;
            this.lockY = true;
        }

         // each frame we spin the bunny around a bit
         this.sprite.x += this.sprite.vx ;
         this.sprite.y += this.sprite.vy ;
         this.counters.a++;
         this.counters.b++;
    }

    // отправляет к указанным координатам
    goToCords(x, y) {
        let toX = x - this.sprite.x;
        let toY = y - this.sprite.y;
        let toPointLength = this.getDistanceTo(toX, toY);

        if (toPointLength == 0) {
            this.sprite.vx = this.sprite.vy = 0;
        } else {
            let toPointX = toX / toPointLength;
            let toPointY = toY / toPointLength;
            this.sprite.vx = this.speed * toPointX;
            this.sprite.vy = this.speed * toPointY;
        }
    }

    //получение координат
    getCords() {
        return {x: this.sprite.x, y: this.sprite.y}
    }

    getDistanceTo(x, y) {
        return Math.sqrt(x * x + y * y);
    }

    getCurRes() {
        return this.currRes;
    }

    switchRes() {
        if (this.currRes == 'a') {
            this.currRes = 'b';
            this.counters.a = 0;
            this.sprite.tint =  0xFF00FF;
        } else if (this.currRes == 'b') {
            this.currRes = 'c';
            this.counters.b = 0;
            this.sprite.tint =  0xFFFF00;
        } else {
            this.currRes = 'a';
            this.counters.c = 0;
            this.sprite.tint =  0x00FF00;
        }
        this.sprite.vx = this.sprite.vx * -1;
        this.sprite.vy = this.sprite.vy * -1;
    }
}