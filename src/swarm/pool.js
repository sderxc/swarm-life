import Item from './item.js';
import Resource from './../resources/resource.js';
import * as PIXI from 'pixi.js';

export default class SwarmPool {
    constructor(swarmCount, app) {
        let gr = new PIXI.Graphics();
        gr.beginFill(0xFFFFFF);
        gr.lineStyle(0);
        gr.drawCircle(1, 1, 2);
        gr.endFill();

        const texture = app.renderer.generateTexture(gr);

        this.pool = [];
        let i = 0
        while (i < swarmCount) {
            const dotSprite = new PIXI.Sprite(texture);
            const item = new Item(dotSprite, app.renderer.width, app.renderer.height);
            app.stage.addChild(item.sprite);
            this.pool.push(item);
            i++;
        }

        // ресурсы 
        this.resources = new Map();
        gr = new PIXI.Graphics();
        gr.beginFill(0xFFFFFF);
        gr.lineStyle(0);
        gr.drawCircle(1, 1, 50);
        gr.endFill();

        let res = new Resource(
            new PIXI.Sprite(app.renderer.generateTexture(gr)),
            Math.floor(Math.random() * app.renderer.width),
            Math.floor(Math.random() * app.renderer.height),
            'a',
            0xFFFF00
        );
        app.stage.addChild(res.sprite);
        this.resources.set(res.name, res);

        res = new Resource(
            new PIXI.Sprite(app.renderer.generateTexture(gr)),
            Math.floor(Math.random() * app.renderer.width),
            Math.floor(Math.random() * app.renderer.height),
            'b', 0xFF00FF
        );
        app.stage.addChild(res.sprite);
        this.resources.set(res.name, res);

        res = new Resource(
            new PIXI.Sprite(app.renderer.generateTexture(gr)),
            Math.floor(Math.random() * app.renderer.width),
            Math.floor(Math.random() * app.renderer.height),
            'c', 0x00FF00
        );
        app.stage.addChild(res.sprite);
        this.resources.set(res.name, res);
    }

    turn() {
        this.shuffle(this.pool);
        for (let i in this.pool) {
            const current = this.pool[i];
            //
            const curRes = current.getCurRes();

            let toResX = this.resources.get(curRes).x - current.sprite.x;
            let toResY = this.resources.get(curRes).y - current.sprite.y;

            if (current.getDistanceTo(toResX, toResY) <= 50) {
                current.switchRes();
            };

            for (let n in this.pool) {
                const another = this.pool[n];
                const anotherCur = another.getCurRes();
                if (n != i && (current.counters[anotherCur] + 50) < another.counters[anotherCur]) {
                    let { x, y } = another.getCords();
                    let toX = x - current.sprite.x;
                    let toY = y - current.sprite.y;
                    if (current.getDistanceTo(toX, toY) <= 50) {
                        //тут надо обменяться координатами
                        another.counters[anotherCur] = current.counters[anotherCur] + 50;
                        another.goToCords(current.sprite.x, current.sprite.y);
                    }
                }
            }

            current.move();
        }
    }
    shuffle(array) {
        array.sort(() => Math.random() - 0.5);
    }
}