import * as PIXI from 'pixi.js';
import SwarmPool from '../src/swarm/pool.js';


// The application will create a renderer using WebGL, if possible,
// with a fallback to a canvas render. It will also setup the ticker
// and the root stage PIXI.Container
const app = new PIXI.Application();

// The application will create a canvas element for you that you
// can then insert into the DOM
document.body.appendChild(app.view);

// load the texture we need
app.loader.load((loader, resources) => {
    const pool = new SwarmPool(300, app);

    // Listen for frame updates
    app.ticker.add(() => {
        pool.turn();
    });
});